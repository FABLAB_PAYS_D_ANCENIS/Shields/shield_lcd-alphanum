#ifndef CLCDALPHANUM_H
#define CLCDALPHANUM_H

#include "generic.h"
#include "LiquidCrystal.h"
#include "math.h"

class CLcdAlphaNum
{
public:
    CLcdAlphaNum();
    void writeLine(char *text);
    void writeLine(uint16 val);
    void writeLine(uint32 val);
    void writeLine(int16 val);
    void writeLine(int32 val);
    void writeLine(uint8 val);
    void writeLine(int8 val);
	void writeLine(bool val);
	void writeLine(double val);
    void setCursor(uint8 col, uint8 row);
    void clear();
    void clearLine(uint8 line);
	void clearChar(uint8 col, uint8 row);
	void diplayCursor(bool cur);
	void createSpecialChar(uint8 u8Char, uint8 u8Line1, uint8 u8Line2, uint8 u8Line3, uint8 u8Line4, uint8 u8Line5, uint8 u8Line6, uint8 u8Line7, uint8 u8Line8);
	void writeSpecialChar(uint8 u8Char);

private:
    LiquidCrystal *lcd;
};

#endif // CLCDALPHANUM_H
