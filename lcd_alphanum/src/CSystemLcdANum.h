#ifndef CSYSTEMLCDANUM_H
#define CSYSTEMLCDANUM_H

#include "generic.h"

extern char m_u8PcbRevision;

class CSystemLcdANum
{
public:
    CSystemLcdANum();
    void configure(void);
    void check(void);
    void disableErrors(void);
	void error(uint8 u8Err);

private:
    bool bErrorDisabled;
};

#endif // CSYSTEMLCDANUM_H
