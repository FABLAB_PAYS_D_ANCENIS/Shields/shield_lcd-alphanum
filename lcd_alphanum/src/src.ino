#include <SPI.h>
#include <SoftwareSerial.h>
#include "generic.h"
#include "CRGBLedShield.h"
#include "CButtonsLcd.h"
#include "CBuzzer.h"
#include "CRadioFreq.h"
#include "CSystemLcdANum.h"
#include "CLcdAlphaNum.h"
//#include "CWifi.h"

// Classes
CRgbLedShield  *RgbLed;
CButtonsLcd    *Button;
CBuzzer        *Buzzer;
CRadioFreq     *RadioFreq;
CSystemLcdANum *System;
CLcdAlphaNum   *LcdAlphaNum;
//CWifi          *Wifi;

// Variables
uint32 u32Count;
uint32 u32Tempo;
uint32 u32Tmp;
uint32 u32Counter;
bool   bOldBpUp;
bool   bOldBpDown;
bool   bOldBpRight;
bool   bOldBpLeft;
uint8  u8BpType;
uint8  u8OldBpType;
uint8  u8MainStep;
uint8  u8AuxStep;
uint8  u8ColorR, u8ColorG, u8ColorB;

// Enums
enum {cStepLedRgb=0,
      cStepBuzzer,
      cStepBp,
      cStepRfTx,
      cStepRfRx,
      //cStepWifi
      };

/*****************************************************************************
 * Function    : setup                                                       *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void setup()
{
    // Serial
    Serial.begin(9600);

    // Instanciate classes
    System      = new CSystemLcdANum();
    RgbLed      = new CRgbLedShield(A3);
    Button      = new CButtonsLcd();
    Buzzer      = new CBuzzer(9);
    RadioFreq   = new CRadioFreq(A0,A1);
    LcdAlphaNum = new CLcdAlphaNum();
    //Wifi        = new CWifi(10,8);

    System->configure();
    System->disableErrors();

    // Init variables
    u32Count   = 0;
    bOldBpUp   = 0;
    u8MainStep = cStepLedRgb;
    u8AuxStep  = 0;
    u32Counter = 0;
    u32Tempo   = millis();
    u8OldBpType = 0;        
}

/*****************************************************************************
 * Function    : loop                                                        *
 *...........................................................................*
 * Description : The loop function runs over and over again forever          *
 *****************************************************************************/
void loop()
{
    int32  s32Tmp;
    int16  s16Tmp;
    uint32 u32Tmp;

    System->check();

    // Step machine
    switch(u8MainStep)
    {
        /* ------------------------  Leds RGB -----------------------------*/
        case cStepLedRgb:
            LcdAlphaNum->setCursor(1,1);
            LcdAlphaNum->writeLine((char*)("Test Led RVB"));
            
            if((millis() - u32Tempo) > 50)
            {
                RgbLed->switchOnLedRgb(u8ColorR, u8ColorG, u8ColorB);

                if(u8AuxStep < 9)
                {
                   u8AuxStep++;
                }
                else
                {
                   u8AuxStep = 0;
                   u8ColorR  = random(0,2)*10;
                   u8ColorG  = random(0,2)*10;
                   u8ColorB  = random(0,2)*10;

                   while(u8ColorR == 0 && u8ColorG == 0 && u8ColorB == 0)
                   {
                       u8ColorR  = random(0,2)*10;
                       u8ColorG  = random(0,2)*10;
                       u8ColorB  = random(0,2)*10;
                   }
                }

                u32Tempo = millis();
            }
        break;

        /* -------------------------  Buzzer -----------------------------*/
        case cStepBuzzer:
            LcdAlphaNum->setCursor(1,1);
            LcdAlphaNum->writeLine((char*)("Test Buzzer"));
            if((millis() - u32Tempo) > 300)
            {
                Buzzer->playNote(random(0,6));

                u32Tempo = millis();
            }
        break;

        /* -----------------------  Buttons ------------------------------*/
        case cStepBp:
            LcdAlphaNum->setCursor(1,1);
            LcdAlphaNum->writeLine((char*)("Test Boutons"));
            
            /*if(Button->getEdgeValue(cBpUp) == 1)
            {
                u8BpType = 1;
                LcdAlphaNum->clearLine(2);
                LcdAlphaNum->setCursor(1,2);
                LcdAlphaNum->writeLine((char*)("Bouton haut"));
            }
            else */if(Button->getEdgeValue(cBpDown) == 1)
            {
                u8BpType = 2;
                LcdAlphaNum->setCursor(1,2);
                LcdAlphaNum->clearLine(2);
                LcdAlphaNum->writeLine((char*)("Bouton bas"));
            }
            else if(Button->getEdgeValue(cBpRight) == 1)
            {
                u8BpType = 3;
                LcdAlphaNum->setCursor(1,2);
                LcdAlphaNum->clearLine(2);
                LcdAlphaNum->writeLine((char*)("Bouton droit"));
            }
            else if(Button->getEdgeValue(cBpLeft) == 1)
            {
                u8BpType = 4;
                LcdAlphaNum->setCursor(1,2);
                LcdAlphaNum->clearLine(2);
                LcdAlphaNum->writeLine((char*)("Bouton gauche"));
            }
            u8OldBpType = u8BpType;
            

        break;

        /* ------------------  Radio-frequency ---------------------------*/
        case cStepRfTx:
            if(u8AuxStep == 0)
            {
                LcdAlphaNum->setCursor(1,1);
                LcdAlphaNum->writeLine((char*)("Test Emission RF"));
                if((millis() - u32Tempo) > 700)
                {
                    u8AuxStep = 1;
                    RadioFreq->init(1);
                    RadioFreq->destAddress(0);

                    s16Tmp = random(0,10999)-1000;
                    LcdAlphaNum->clearLine(2);
                    LcdAlphaNum->setCursor(1,2);
                    LcdAlphaNum->writeLine(s16Tmp);
                    RadioFreq->send(s16Tmp, 1);
                    u32Tempo = millis();
                }
            }
            else
            {
                if((millis() - u32Tempo) > 3000)
                {
                    s16Tmp = random(0,10999)-1000;
                    LcdAlphaNum->clearLine(2);
                    LcdAlphaNum->setCursor(1,2);
                    LcdAlphaNum->writeLine(s16Tmp);
                    RadioFreq->send(s16Tmp, 0);
                    u32Tempo = millis();
                }
            }

        break;

        /* ------------------  Radio-frequency ---------------------------*/
        case cStepRfRx:
            if(u8AuxStep == 0)
            {
                  u8AuxStep = 1;
                  LcdAlphaNum->setCursor(1,1);
                  LcdAlphaNum->writeLine((char*)("Test Reception RF"));
                  RadioFreq->init(0);
                  RadioFreq->destAddress(1);

            }
            else
            {
                if((millis() - u32Tempo) > 500)
                {
                    if(RadioFreq->isCodeReceived())
                    {
                      s32Tmp = RadioFreq->receiveCode();
                      LcdAlphaNum->clearLine(2);
                      LcdAlphaNum->setCursor(1,2);
                      LcdAlphaNum->writeLine(s32Tmp);
                    }
                    u32Tempo = millis();
                }
            }

        break;

        /* ------------------  Wifi ---------------------------*/
        /*case cStepWifi:
            if(u8AuxStep == 0)
            {
                u8AuxStep = 1;
              
                LcdAlphaNum->setCursor(1,1);
                LcdAlphaNum->writeLine((char*)("Test Wifi"));                

                LcdAlphaNum->setCursor(1,2);
                LcdAlphaNum->writeLine((char*)("Connexion..."));

                Wifi->connect((char*)("Fablab"), (char*)("Fablab44"));

                LcdAlphaNum->clearLine(2);
                LcdAlphaNum->setCursor(1,2);
                LcdAlphaNum->writeLine((char*)("Connecte"));

                Wifi->loginFree((char*)(" "), (char*)(" "));
            }
            else
            {
                if(Button->getEdgeValue(cBpDown) == 1)
                {
                   u8AuxStep++;
                   //Wifi->send(u8AuxStep);
                   Wifi->sendSms((char*)("Hello !"));
                }
             }
        break;*/

        /* ------------------------- Default ----------------------------*/
        default:
            // Init variables
            u32Count = 0;
            bOldBpUp = 0;
            u8MainStep = cStepLedRgb;
            u8AuxStep = 0;
            u32Tmp = 0;
            u32Tempo = millis();
            RadioFreq->stop();
        break;
    }

    /* ---------------- Increment step machine ------------------------*/
    if((Button->getEdgeValue(cBpUp) == 1))
    {
        u8MainStep++;
        u32Tempo = millis();
        u32Tmp = 0;
        u8AuxStep = 0;

        // Stop all devices
        RgbLed->switchOffLed();
        Buzzer->stopNote();
        LcdAlphaNum->clear();
        
    }
    bOldBpUp    = Button->getValue(cBpUp);
    bOldBpDown  = Button->getValue(cBpDown);
    bOldBpRight = Button->getValue(cBpRight);
    bOldBpLeft  = Button->getValue(cBpLeft);
}
