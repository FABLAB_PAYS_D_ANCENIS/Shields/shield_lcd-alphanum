#include "CLcdAlphaNum.h"


/*****************************************************************************
 * Function    : CLcdAlphaNum                                                *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
CLcdAlphaNum::CLcdAlphaNum()
{
   // initialize the library with the numbers of the interface pins
   const int rs = 7, en = 6, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
   lcd = new LiquidCrystal(rs, en, d4, d5, d6, d7);

   // set up the LCD's number of columns and rows:
   lcd->begin(16, 2);
   lcd->noAutoscroll();
   lcd->clear();
}

/*****************************************************************************
 * Function    : writeLine                                                   *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CLcdAlphaNum::writeLine(char *text)
{
  lcd->print(text);
}

void CLcdAlphaNum::writeLine(uint16 val)
{
  lcd->print(val);
}

void CLcdAlphaNum::writeLine(uint32 val)
{
  lcd->print(val);
}

void CLcdAlphaNum::writeLine(int16 val)
{
  lcd->print(val);
}

void CLcdAlphaNum::writeLine(int32 val)
{
  lcd->print(val);
}

void CLcdAlphaNum::writeLine(uint8 val)
{
  lcd->print(val);
}

void CLcdAlphaNum::writeLine(int8 val)
{
  lcd->print(val);
}

void CLcdAlphaNum::writeLine(bool val)
{
  lcd->print(val);
}

void CLcdAlphaNum::writeLine(double val)
{
  lcd->print(int32(val));
}

/*****************************************************************************
 * Function    : setCursor                                                   *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CLcdAlphaNum::setCursor(uint8 col, uint8 row)
{
    lcd->setCursor(col-1, row-1);
}

/*****************************************************************************
 * Function    : clear                                                       *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CLcdAlphaNum::clear(void)
{
    lcd->clear();
}

/*****************************************************************************
 * Function    : clear                                                       *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CLcdAlphaNum::clearLine(uint8 u8line)
{
    this->setCursor(1, u8line);
    lcd->print("                ");
    this->setCursor(1, u8line);
}

/*****************************************************************************
 * Function    : clear                                                       *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CLcdAlphaNum::clearChar(uint8 col, uint8 row)
{
    this->setCursor(col, row);
    lcd->print(" ");
}

/*****************************************************************************
 * Function    : createChar                                                  *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CLcdAlphaNum::createSpecialChar(uint8 u8Char, uint8 u8Line1, uint8 u8Line2, uint8 u8Line3, uint8 u8Line4, uint8 u8Line5, uint8 u8Line6, uint8 u8Line7, uint8 u8Line8)
{
	uint8  tu8Char[8] = {u8Line1,u8Line2,u8Line3,u8Line4,u8Line5,u8Line6,u8Line7,u8Line8};
	lcd->createChar(u8Char, tu8Char);
	
	// Call clear function to avoid bug: no display after createchar
	this->clear();
}

/*****************************************************************************
 * Function    : createChar                                                  *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CLcdAlphaNum::writeSpecialChar(uint8 u8Char)
{
	lcd->write(byte(u8Char));
}

/*****************************************************************************
 * Function    : diplayCursor                                                *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CLcdAlphaNum::diplayCursor(bool cur)
{
	if(cur)
		lcd->cursor();
	else
		lcd->noCursor();
}

