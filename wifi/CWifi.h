#ifndef CWIFI_H
#define CWIFI_H

#include "generic.h"
#include "WiFiEsp.h"
#include <SoftwareSerial.h>

class CWifi
{
public:
    CWifi(uint8 u8PinTx, uint8 u8PinRx);
    void connect(char *ssid, char *pass);
    bool isConnected();
    void send(uint8 value);
    void loginFree(char *login, char *pass);
    void sendSms(char *login);

private:
    
	uint8 m_u8PinTx, m_u8PinRx;
    SoftwareSerial *SerialWifi;
    int status;
    char ssid[26];
    char pass[26];
    char loginSms[26];
    char passSms[26];
    bool connectedWifi;

};

#endif // CWIFI_H
