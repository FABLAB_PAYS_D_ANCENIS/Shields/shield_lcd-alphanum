#include "CWifi.h"


char server[] = "fablabpaysancenis.free.fr";
char serverSmsFree[] = "fablabpaysancenis.free.fr";

// Initialize the Ethernet client object
WiFiEspClient client;

/*****************************************************************************
 * Function    : CWifi                                                       *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
CWifi::CWifi(uint8 u8PinTx, uint8 u8PinRx)
{
	// Store pins
	m_u8PinTx = u8PinTx;
	m_u8PinRx = u8PinRx;
	
    // The Wifi radio's status
    status = WL_IDLE_STATUS;

    // Init flag
    connectedWifi = false;
}

/*****************************************************************************
 * Function    : connect                                                     *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CWifi::connect(char *ssid, char *pass)
{
    // Initialize serial for ESP module
    SerialWifi = new SoftwareSerial(m_u8PinTx, m_u8PinRx);
    SerialWifi->begin(115200);

    // initialize ESP module
    WiFi.init(SerialWifi);   
    
    // attempt to connect to WiFi network
    while (status != WL_CONNECTED) 
    {
        Serial.print("[WiFiEsp] Attempting to connect to ");
        Serial.println(ssid);
      
        // Connect to WPA/WPA2 network
        status = WiFi.begin(ssid, pass);
    }

    // Set connected
    connectedWifi = true;
}

/*****************************************************************************
 * Function    : isConnected                                                 *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
bool CWifi::isConnected(void)
{
    return connectedWifi;
}

/*****************************************************************************
 * Function    : send                                                        *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CWifi::send(uint8 value)
{
    // Connect
    if (client.connect(server, 80)) 
    {
        // Debug
        Serial.print("[WiFiEsp] Connected to server ");
        Serial.println(server);
        
        // Make a HTTP request
        // "GET /remoteTest.php?ok=1&value=3200 HTTP/1.1"
        client.print("GET /remoteTest.php?ok=1&value=");
        client.print(value);
        client.println(" HTTP/1.1");
        client.println("Host: fablabpaysancenis.free.fr");
        client.println("Connection: close");
        client.println();
    }
}

/*****************************************************************************
 * Function    : loginFree                                                   *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CWifi::loginFree(char *login, char *pass)
{
    strcpy(loginSms, login);
    strcpy(passSms, pass);
}

/*****************************************************************************
 * Function    : sendSms                                                     *
 *...........................................................................*
 * Description :                                                             *
 *****************************************************************************/
void CWifi::sendSms(char *message)
{
    // Connect
    if (client.connect(server, 80)) 
    {
        // Debug
        Serial.print("[WiFiEsp] Connected to server ");
        Serial.println(server);
        
        // Make a HTTP request
        // "GET /remoteTest.php?ok=1&value=3200 HTTP/1.1"
        client.print("GET /sendmsg?user=");
        client.print(loginSms);
        client.print("&pass=");
        client.print(passSms);
        client.print("&msg=");
        client.print(message);
        client.println(" HTTP/1.1");
        client.println("Host: smsapi.free-mobile.fr");
        client.println("Connection: close");
        client.println();
    }
}

    
